package dk.dren.doorbell.bell;

import java.util.Map;
import java.util.TreeMap;

public class Bells {
    private final Map<String, Bell> bells = new TreeMap<>();

    public synchronized Bell get(String id) {
        return bells.computeIfAbsent(id, i->new Bell());
    }
}
