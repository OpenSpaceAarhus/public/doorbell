package dk.dren.doorbell.bell;

import lombok.Getter;

@Getter
public class Bell {
    long lastRinging;

    public void ring() {
        lastRinging = System.currentTimeMillis();
    }
}
